<?php
require_once __DIR__."/../../helper/init.php";
$page_title ="Quick ERP | ADD SALES";
    $sidebarSection = 'transaction';
    $sidebarSubSection = 'sales';
    Util::createCSRFToken();
  $errors="";
  $old="";
  if(Session::hasSession('old'))
  {
    $old = Session::getSession('old');
    Session::unsetSession('old');
  }
  if(Session::hasSession('errors'))
  {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <?php
    require_once __DIR__."/../includes/head-section.php";
  ?>
  <style>
      .email-verify{
          background: green;
          color: #FFF;
          padding: 5px 10px;
          font-size: .875rem;
          line-height: 1.5;
          border-radius: .2rem;
          vertical-align: middle;
          display: none;
      }

  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
        <?php require_once __DIR__."/../includes/navbar.php"; ?>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content-->
        
        <!-- Page Heading -->
        <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-4 text-gray-800">Sales</h1>
            </div>
        </div>
        <!-- /.container-fluid -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card show mb-4">
                        <!--CARD HEADER-->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-end">
                            <div class="mr-3">
                            <input type="text" class="form-control" 
                                    name="email" id="customer_email" 
                                    placeholder="Enter Email Of Customer">
                            </div>
                            <div>
                                <p class="email-verify" id="email_verify_success">
                                <i class="text-white mr-1 fas fa-check fa-sm"></i> Email Verified</p>

                                <p class="email-verify bg-danger mb-0" id="email_verify_fail">
                                <i class="text-white mr-1 fas fa-times fa-sm"></i> Email Not Verified</p>
                            
                                <a href="<?=BASEPAGES;?>add-customer.php" class="btn btn-warning btn-sm shadow-sm " id="add_customer_btn"  style="display:none!important"> <i class="fa fa-user fa-sm text-white "></i> Add Customer</a>

                                <button type="button" class="btn btn-primary shadow-sm"
                                name="check_email" id="check_email"><i class="fa fa-envelope fa-sm text-white "></i> Check Email</button>

                            </div>
                        </div>
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">
                                <i class="fas fa-plus"> Sales</i>
                            </h6>
                            <button type="button" 
                                    onclick="addProduct();"
                                    class="d-sm-inline-block btn btn-sm btn-secondary shadow-sm" id="add-product" disabled>
                                    <i class="fa fa-plus fa-sm text-white "></i>
                                     Add One More Product</button>
                        </div>
                        <!--END OF CARD HEADER-->

                        <!--CARD BODY-->
                        <div class="card-body">
                          <form id="add-sales" action="<?= BASEURL?>helper/routing.php" method="POST">
                            <input type="hidden" name="customer_id" id="customer_id" >
                            <input type="hidden"
                              name="csrf_token"
                              value="<?= Session::getSession('csrf_token');?>">
                            
                            <div id="products_container">
                                <!--BEGIN PRODUCT CUSTOM CONTROL-->
                                <div class="row product_row" id="element_1">
                                    <!--BEGIN CATEGORY SELECT-->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Category</label>
                                            <select id="category_1" class="form-control category_select">
                                                <option disbaled selected>Select Category</option>
                                                <?php 
                                                $categories = $di->get("database")->readData("category",["id","name"],"deleted=0");
                                                foreach($categories as $category){
                                                  echo "<option value='{$category->id}'>{$category->name}</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--END CATEGORY SELECT-->
                                    <!--BEGIN PRODUCTS SELECT-->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Products</label>
                                            <select name="product_id[]" id="product_1" class="form-control product_select">
                                                <option disbaled selected>Select Product</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <!--END PRODUCTS SELECT-->
                                    <!--BEGIN SELLING PRICE-->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Selling Price</label>
                                            <input type="number" name="selling_price[]" id="selling_price_1" value="0"
                                                    class="form-control " disabled>
                                        </div>
                                    </div>
                                    <!--END SELLING PRICE-->
                                    <!--BEGIN QUANTITY-->
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="">Quantity</label>
                                            <input type="number" name="quantity[]" id="quantity_1"
                                                    class="form-control quantity_select"
                                                    value="0">
                                        </div>
                                    </div>
                                    <!--END QUANTITY-->
                                    <!--BEGIN DISCOUNT-->
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="">Discount</label>
                                            <input type="number" max="100" name="discount[]" id="discount_1"
                                                    class="form-control discount_select"
                                                    value="0">
                                        </div>
                                    </div>
                                    <!--END DISCOUNT-->
                                    <!--BEGIN SELLING PRICE-->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Final Rate</label>
                                            <input type="text"  name = "final_rate[]" id="final_rate_1" value="0"
                                                    class="form-control" disabled>
                                        </div>
                                    </div>
                                    <!--END SELLING PRICE-->
                                    <!--BEGIN DELETE BUTTON-->
                                    <div class="col-md-1">
                                        <button onclick="deleteProduct(1)"
                                                type="button"
                                                class="btn btn-danger"
                                                style="margin-top: 45%;">
                                                <i class="far fa-trash-alt"></i>            
                                        </button>
                                    </div>
                                    <!--END DELETE BUTTON-->
                                </div>
                                <!--END PRODUCT CUSTOM CONTROL-->
                            </div>                          
                        </div>
                        <!--END OF CARD BODY-->
                        <!--BEGIN: CARD FOOTER-->
                        <div class="card-footer d-flex justify-content-between">
                            <div>
                                <input type="submit" class="btn btn-primary" disabled name="add_sales" value="Submit" id="add_sales">
                            </div>
                            <div class="form-group row">
                                <label for="finalTotal" class="col-sm-4 col-form-label">Final Total</label>
                                <div class="col-sm-8">
                                    <input type="number" disabled class="form-control" id="finalTotal" value="0">
                                </div>
                            </div>
                        </div>
                        <!--END: CARD FOOTER-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
      <!-- End of Main Content -->
</div>
      <!-- Footer -->
      <?php require_once __DIR__."/../includes/footer.php"; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  
  <?php require_once __DIR__."/../includes/scroll-to-top.php"; ?>
  <?php require_once __DIR__."/../includes/core-scripts.php"; ?>

  <script src="<?=BASEASSETS?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
  <script src="<?=BASEASSETS?>js/pages/transaction/add-sales.js"></script>


</body>

</html>
