<?php
require_once __DIR__."/../../helper/init.php";
$page_title ="Quick ERP | ADD product";
    $sidebarSection = 'product';
    $sidebarSubSection = 'edit';
    Util::createCSRFToken();
  $errors="";
  $old="";
  if(Session::hasSession('old'))
  {
    $old = Session::getSession('old');
    Session::unsetSession('old');
  }
  if(Session::hasSession('errors'))
  {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
  }
  $prod_id = $_GET['id'];
  $prod_details = $di->get('product')->getProductByProductId($prod_id,PDO::FETCH_ASSOC);
  $c = count($prod_details)-1;
  // Util::dd();

  $query = "SELECT concat(suppliers.first_name,\" \",suppliers.last_name) as supplier_name ,suppliers.id from suppliers";
  $suppliers = $di->get('database')->raw($query,PDO::FETCH_ASSOC);
 
  $count = count($prod_details);

  $prod_array = [];
  foreach($prod_details as $prod){
   
    array_push($prod_array,$prod["supplier_name"]);
  }
  // Util::dd($prod_details[0]);
  
  // Util::Dd($suppliers[6]['supplier_name']);
  
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <?php
    require_once __DIR__."/../includes/head-section.php";
  ?>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
  

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
        <?php require_once __DIR__."/../includes/navbar.php"; ?>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content-->
        
        <!-- Page Heading -->
        <style>
          .hidden{
            display: none;
          }
        </style>
        <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Edit product</h1>
                <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                    <i class="fas fa-list-ul fa-sm text-white"></i>Manage product</a>
            </div>
        </div>
        <!-- /.container-fluid -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card show mb-4">
                        <div class="card-header">
                            <h6 class="m-0 font-weight-bold text-primary">
                                    <i class="fa fa-pen"></i> Edit product
                            </h6>
                        </div>
                        <!--END OF CARD HEADER-->

                        <div class="card-body">
                          <form action="<?= BASEURL;?>helper/routing.php?id={$_GET['id']}" method="GET" id="edit-product">
                          <input type="hidden" name="id" id="edit_product_id" value= <?=$prod_id?>>
                          <input type="hidden" 
                            name="csrf_token"
                            value="<?= Session::getSession('csrf_token');?>"
                          >
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label for="name">Product Name</label>
                                  <input type="text" 
                                    class="form-control" 
                                    name="name"
                                    id="name"  
                                    placeholder="Enter Product Name"
                                    value="<?= $old != '' ?$old['name']: $prod_details[0]['name']?>"
                                  >
                                  <?php
                                  if($errors!="" && $errors->has('name')):
                                    echo "<span class='error'>{$errors->first('name')}</span>";
                                  endif;
                                  ?>
                                </div>
                              </div>
                                

                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">Specification</label>
                                  <input type="text" 
                                    class="form-control" 
                                    name="specification"
                                    id="specification"  
                                    placeholder="Enter Product Specification"
                                    value="<?= $old != '' ?$old['specification']: $prod_details[0]['specification']?>"
                                  >
                                  <?php
                                  if($errors!="" && $errors->has('specification')):
                                    echo "<span class='error'>{$errors->first('specification')}</span>";
                                  endif;
                                  ?>
                              </div>                                
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">HSN Code</label>
                                  <select name="hsn_code" id="hsn_code" class="form-control" disabled>
                                    <?php
                                      $hsn_code = $di->get('database')->readData('gst', ['id', 'hsn_code'], 'deleted = 0');
                                      foreach($hsn_code as $row){
                                        echo "<option value={$row->hsn_code} ";
                                         if($prod_details[0]['hsn_code']==$row->hsn_code)
                                         {
                                           echo "selected";
                                         }
                                         echo ">{$row->hsn_code}</option>";
                                      }
                                    ?>
                                  </select>
                                  <?php
                                  if($errors!="" && $errors->has('hsn_code')):
                                    echo "<span class='error'>{$errors->first('hsn_code')}</span>";
                                  endif;
                                  ?>
                              </div>                                
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">Suppliers</label>
                                  <select name="supplier_id[]" id="supplier_id" class="form-control" multiple>
                                    <?php
                                    
                                     
                                      // Util::dd($suppliers_array);
                                     
                                     $i=0;
                                    
                                      foreach($suppliers as $supplier){
                                        
                                        echo "<option value={$supplier['id']}";
                                       
                                        if($count > 0){
                                          if(in_array($supplier['supplier_name'],$prod_array)){
                                            echo " selected";
                                            $count--; 
                                          }
                                         
                                        }
                                        
                                        echo ">{$supplier['supplier_name']}</option>";
                                        $i++;
                                        
                                      }
                                    ?>
                                  </select>
                              </div>                                
                            </div>

                            <?php
                            // Util::dd($suppliers);
                            // Util::dd($suppliers_array);
                            // Util::dd($prod_details);
                            // Util::dd($suppliers[0]['supplier_name']);
                                // Util::dd(in_array($prod_details[$i]['supplier_name'] , $suppliers));
                            ?>
                             <?php
                                      $categories = $di->get('database')->readData('category', ['id', 'name'], 'deleted = 0');
                                      
                                      ?>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">Category</label>
                                  <select name="category_id" id="category_id" class="form-control" disabled>
                                    <?php
                                      
                                      
                                       foreach($categories as $category){
                                         
                                        echo "<option value={$category->id}";
                                         if($prod_details[0]['category_name']==$category->name)
                                         {
                                           echo " selected";
                                         }
                                         echo ">{$category->name}</option>";
                                      }
                                    ?>
                                  </select>
                              </div>                                
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">Selling Price</label>
                                  <input type="text" 
                                    class="form-control" 
                                    name="selling_rate"
                                    id="selling_rate"  
                                    placeholder="Enter Product Selling Price"
                                    value="<?= $old != '' ?$old['selling_rate']: $prod_details[0]['selling_rate']?>"
                                  >
                              </div>                                
                            </div>

                            <div class="col-md-6" id="wef-div">
                                <div class="form-group">
                                  <label for="name">With Effect From</label>
                                  <input type="text" 
                                    class="form-control" 
                                    name="wef"
                                    id="wef"  
                                    placeholder=""
                                    value="<?= $old != '' ?$old['wef']: $prod_details[0]['wef']?>"
                                    
                                    
                                  >
                              </div>                                
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">EOQ Level</label>
                                  <input type="text" 
                                    class="form-control" 
                                    name="eoq_level"
                                    id="eoq_level"  
                                    placeholder=""
                                    value="<?= $old != '' ?$old['eoq_level']: $prod_details[0]['eoq_level']?>"
                                  >
                              </div>                                
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">Danger Level</label>
                                  <input type="text" 
                                    class="form-control" 
                                    name="danger_level"
                                    id="danger_level"  
                                    placeholder=""
                                    value="<?= $old != '' ?$old['danger_level']: $prod_details[0]['danger_level']?>"
                                  >
                              </div>                                
                            </div>

                           
                            </div>
                            <input type="submit" class="btn btn-primary" name="editProduct" value="submit">
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- End of Main Content -->
</div>
      <!-- Footer -->
      <?php require_once __DIR__."/../includes/footer.php"; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  
  <?php require_once __DIR__."/../includes/scroll-to-top.php"; ?>
  <?php require_once __DIR__."/../includes/core-scripts.php"; ?>

  <?php require_once __DIR__."/../includes/page-level/index-scripts.php"; ?>
  <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
  <script>

var selling_price = document.getElementById('wef-div');
var sp = document.getElementById('selling_rate');
var wef = document.getElementById('wef');
console.log(sp);

sp.addEventListener('change',function(){
  
  selling_price.disabled=false;
  flatpickr("#wef", {
      enableTime: true,    
      minDate:new Date(),
      defaultDate: new Date(wef.value)       
  });

  
});
     
  


  </script>
  
  <script src="<?=BASEASSETS?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
  



</body>

</html>
