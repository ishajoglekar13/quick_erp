<?php
    require_once __DIR__."/../../helper/init.php";
    $page_title ="Quick ERP | Invoice";

    Util::createCSRFToken();
    error_reporting(E_ALL ^ E_WARNING);

    
    $query = "select customer_id from invoice where id = {$_GET['id']}";

    $cust_id = (int)$di->get('database')->raw($query,PDO::FETCH_ASSOC)[0]['customer_id'];
   
    $customer_details = $di->get('customer')->getCustomer($cust_id);


    // $invoiceQuery = "select sales.quantity,sales.discount,products.name,products.specification,products_selling_rate.selling_rate from sales inner join products on sales.product_id = products.id inner join products_selling_rate on products.id = products_selling_rate.product_id where sales.invoice_id={$_GET['id']}";


    $invoiceQuery = "select concat(customers.first_name,\" \",customers.last_name)as customer_name,customers.gst_no,customers.phone_no,customers.email_id,customers.gender, CONCAT(address.block_no,\", \", address.street,\", \", address.city,\",\", address.pincode,\", \", address.town,\",\", address.state,\",\", address.country) as customer_address ,sales.quantity,sales.discount,products.name,products.specification,products_selling_rate.selling_rate from sales inner join products on sales.product_id = products.id inner join products_selling_rate on products.id = products_selling_rate.product_id inner join invoice on invoice.id = sales.invoice_id inner join customers on customers.id = invoice.customer_id INNER join address on address.id = (select address_customer.address_id from address_customer where address_customer.customer_id = invoice.customer_id) where sales.invoice_id={$_GET['id']}";
   
    $invoiceQueryResult = $di->get('database')->raw($invoiceQuery,PDO::FETCH_ASSOC);  
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <?php
    require_once __DIR__."/../includes/head-section.php";
  ?>
  

</head>

<body id="page-top">

<style>
    body {
    background-color: #000
}

.padding {
    padding: 2rem !important
}

.card {
    margin-bottom: 30px;
    border: none;
    -webkit-box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
    -moz-box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
    box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22)
}

.card-header {
    background-color: #fff;
    border-bottom: 1px solid #e6e6f2
}

h3 {
    font-size: 20px
}

h5 {
    font-size: 15px;
    line-height: 26px;
    color: #3d405c;
    margin: 0px 0px 15px 0px;
    font-family: 'Circular Std Medium'
}

.text-dark {
    color: #3d405c !important
}
.table td{
    margin-top: -0.5rem;
    border-top: none!important;
}

td{
    text-align: center;
}
.spec{
    text-align: left;
}
</style>
  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
        <?php require_once __DIR__."/../includes/navbar.php"; ?>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <!-- Page Heading -->
        <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                
                <!-- 
                    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
                    <div class="offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 padding">
     <div class="card">
         <div class="card-header p-4">
          
             <div class="float-right">
                 <h3 class="mb-0">Invoice #<?=$_GET['id']?></h3>
                 Date: 12 Jun,2019
             </div>
         </div>
         <div class="card-body">
             <div class="row mb-4">
                 <div class="col-sm-6">
                     <h3 class="mb-3">Details:</h3>
                  
               
                     <div class="text-dark mb-1"><?=$invoiceQueryResult[0]['customer_name']?></div>
               
               <div>GST NO : <strong><?=$invoiceQueryResult[0]['gst_no']?></strong></div>
               
               <div>Address : <br><?=$invoiceQueryResult[0]['customer_address']?></div>
               <div>Email : <?=$invoiceQueryResult[0]['email_id']?></div>
               <div>Phone : <?=$invoiceQueryResult[0]['phone_no']?></div>
                 </div>
                
             </div>
             <div class="table-responsive-sm">
                 <table class="table table-striped">
                     <thead>
                         <tr>
                             <th class="center">#</th>
                             <th>Product</th>
                             <th>Specification</th>
                             <th class="right">Price</th>
                             <th class="center">Quantity</th>
                             <th class="center">Discount</th>
                             
                             <th class="right">Total</th>
                         </tr>


                         <tr>

                         <?php
                         $finalTotal  =0;
                            for($i=0;$i<count($invoiceQueryResult);$i++)
                            {

                            

                            $total =  round((int)$invoiceQueryResult[$i]['selling_rate']*(int)$invoiceQueryResult[$i]['quantity']-((int)$invoiceQueryResult[$i]['selling_rate']*(int)$invoiceQueryResult[$i]['quantity'])/(100/(int)$invoiceQueryResult[$i]['discount']),2);
                            
                            $finalTotal += $total;


                      
                            // Util::Dd($total);
                                ?>
                                <tr>
                                <td><?=$i+1?></td>
                                <td><?=$invoiceQueryResult[$i]['name']?></td>
                                <td class="spec"><?= $invoiceQueryResult[$i]['specification']?></td>
                                <td><?= $invoiceQueryResult[$i]['selling_rate']?></td>
                                <td><?= $invoiceQueryResult[$i]['quantity']?></td>
                                <td><?=$invoiceQueryResult[$i]['discount']?></td>
                                <td><?=$total?></td>
                                </tr>

                                <?php
                            }

                        ?>

                         </tr>
                     </thead>
                     <tbody>
                     
                     </tbody>
                 </table>
             </div>
             
         </div>
         <div class="card-footer bg-white">
         <div class="row">
               
                 <div class="col-lg-4 col-sm-5 ml-auto">
                     <table class="table table-clear">
                         <tbody>
                             <tr>
                                 <td>
                                     <strong class="text-dark">Final Total :</strong> </td>
                                 <td class="left">
                                <strong class="text-dark">	&#x20B9; <?= $finalTotal ?></strong>
                                 </td>
                             </tr>
                         </tbody>
                     </table>
                 </div>
             </div>
         </div>
     </div>
 </div>
            </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once __DIR__."/../includes/footer.php"; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  
  <?php require_once __DIR__."/../includes/scroll-to-top.php"; ?>
  <?php require_once __DIR__."/../includes/core-scripts.php"; ?>

  <?php require_once __DIR__."/../includes/page-level/index-scripts.php"; ?>

</body>

</html>

<!-- 1.customer details
2.prod details
3. -->