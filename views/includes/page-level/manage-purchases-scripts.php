
<script src ="<?=BASEASSETS?>js/plugins/toastr/toastr.min.js"></script>

<script src="<?=BASEASSETS?>vendor/datatables/datatables.min.js"></script>



<script>

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

<?php

    if(Session::hasSession(REQUEST_SENT)):
    
?>
    toastr.success("Request has been Sent to supplier","Sent");
<?php
    Session::unsetSession(REQUEST_SENT);
    elseif(Session::hasSession('csrf')):
        ?>
        
        toastr.error("Unauthorized Access, Token Mismatch","Unauthorized Access");
        
        <?php
        Session::unsetSession('csrf');

    endif;
?>


</script>