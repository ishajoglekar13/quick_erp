var id = 1;

var baseURL = window.location.origin;
var filePath = "/helper/routing.php";

function deleteProduct(delete_id){
    var elements = document.getElementsByClassName("product_row");
    if(elements.length != 1){
        $("#element_"+delete_id).remove();
        
        setFinalTotal();
    }
   
}
function addProduct(){
    id++;
    $("#products_container").append(` 
    <!--BEGIN PRODUCT CUSTOM CONTROL-->
    <div class="row product_row" id="element_`+id+`">
        <!--BEGIN CATEGORY SELECT-->
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Category</label>
                <select id="category_`+id+`" class="form-control category_select">
                    <option disbaled selected>Select Category</option>
                </select>
            </div>
        </div>
        
        <!--END CATEGORY SELECT-->
      
        <!--BEGIN PRODUCTS SELECT-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Products</label>
                <select name="product_id[]" id="product_`+id+`" class="form-control product_select">
                    <option disbaled selected>Select Product</option>
                    
                </select>
            </div>
        </div>

        <!--END PRODUCTS SELECT-->

        <div class="col-md-3">
        <div class="form-group">
            <label for="">Suppliers</label>
            <select name="supplier_id[]" id="supplier_`+id+`" class="form-control supplier_select">
                <option disbaled selected>Select Supplier</option>
        
            </select>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <label for="purchase_rate">Purchase Rate</label>
            <input type="number" name="purchase_rate[]" id="purchase_rate_`+id+`" value="0" class="form-control">
        </div>
    </div>

  
        <!--BEGIN QUANTITY-->
        <div class="col-md-1">
            <div class="form-group">
                <label for="">Quantity</label>
                <input type="number" name="quantity[]" id="quantity_`+id+`"
                        class="form-control quantity_select"
                        value="0">
            </div>
        </div>
        <!--END QUANTITY-->
        
       
        
    <!--BEGIN DELETE BUTTON-->
    <div class="col-md-1">
        <button onclick="deleteProduct(`+id+`)"
                type="button"
                class="btn btn-danger"
                style="margin-top: 45%;">
                <i class="far fa-trash-alt"></i>            
        </button>
    </div>
    <!--END DELETE BUTTON-->
</div>
<!--END PRODUCT CUSTOM CONTROL-->`
        );

        
                                
        $.ajax({
            url:baseURL + filePath,
            method: 'POST',
            data:{
                getCategories: true
            },
            dataType: 'json',
            success: function(categories){
                categories.forEach(function (category){
                $("#category_"+id).append(
                    `<option value='${category.id}'>${category.name}</option>`
                );
            });
            }
        });
    }

    // using event delegation

$("#products_container").on('change','.category_select', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var category_id = this.value;
    $.ajax({
        url:baseURL + filePath,
        method: 'POST',
        data:{
            getProductsByCategoryID: true,
            categoryID: category_id
        },
        dataType: 'json',
        success: function(products){
            $("#product_"+element_id).empty();
            $("#supplier_"+element_id).empty();
            $("#supplier_"+element_id).append("<option disabled selected>Select Supplier</option>");
            document.getElementById('purchase_rate_'+element_id).value=0;
            $("#product_"+element_id).append("<option disabled selected>Select Product</option>");
            products.forEach(function (product){
                $("#product_"+element_id).append(
                    `<option value='${product.id}'>${product.name}</option>`
                );
            });
           
        }
    })
});
$("#products_container").on('change','.product_select', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var product_id = this.value;
    console.log(product_id);
    $.ajax({
        url:baseURL + filePath,
        method: 'POST',
        data:{
            getSuppliersByProductId: true,
            productID: product_id
        },
        dataType: 'json',
        success: function(suppliers){
            console.log("hey");     
            
            $("#supplier_"+element_id).empty();
            $("#supplier_"+element_id).append("<option disabled selected>Select supplier</option>");
            suppliers.forEach(function (supplier){
                $("#supplier_"+element_id).append(
                    `<option value='${supplier.id}'>${supplier.supplier_name}</option>`
                );
            });
        }
    })
});
$("#products_container").on('change','.product_select', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var product_id = this.value;
    var purchase_rate1 = document.getElementById('purchase_rate_'+element_id);
    console.log(product_id);
    $.ajax({
        url:baseURL + filePath,
        method: 'POST',
        data:{
            getPurchaseRateByProductId: true,
            productID: product_id
        },
        dataType: 'json',
        success: function(purchase_rate){
            console.log(purchase_rate1);   
            purchase_rate = purchase_rate-(20*purchase_rate/100);
            purchase_rate1.value = purchase_rate;  
            
        }
    })
});
$("#products_container").on('change','.quantity_select', function(){
    var element_id = $(this).attr('id').split("_")[1];
            var quantity_dom =  document.getElementById("quantity_"+element_id);
            
            
            quantity_dom.min = 0;
        
            if(quantity_dom.value < 0)
                quantity_dom.value = 0;
      
});






