var id = 1;

var baseURL = window.location.origin;
var filePath = "/helper/routing.php";

function deleteProduct(delete_id){
    var elements = document.getElementsByClassName("product_row");
    if(elements.length != 1){
        $("#element_"+delete_id).remove();
        
        setFinalTotal();
    }
   
}
function addProduct(){
    id++;
    $("#products_container").append(` 
    <!--BEGIN PRODUCT CUSTOM CONTROL-->
    <div class="row product_row" id="element_`+id+`">
        <!--BEGIN CATEGORY SELECT-->
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Category</label>
                <select id="category_`+id+`" class="form-control category_select">
                    <option disbaled selected>Select Category</option>
                </select>
            </div>
        </div>
        <!--END CATEGORY SELECT-->
        <!--BEGIN PRODUCTS SELECT-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Products</label>
                <select name="product_id[]" id="product_`+id+`" class="form-control product_select">
                    <option disbaled selected>Select Product</option>
                    
                </select>
            </div>
        </div>
        <!--END PRODUCTS SELECT-->
        <!--BEGIN SELLING PRICE-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Selling Price</label>
                <input type="number" value="0" name="selling_price[]" id="selling_price_`+id+`"
                        class="form-control" disabled>
            </div>
        </div>
        <!--END SELLING PRICE-->
        <!--BEGIN QUANTITY-->
        <div class="col-md-1">
            <div class="form-group">
                <label for="">Quantity</label>
                <input type="number" name="quantity[]" id="quantity_`+id+`"
                        class="form-control quantity_select"
                        value="0">
            </div>
        </div>
        <!--END QUANTITY-->
        <!--BEGIN DISCOUNT-->
        <div class="col-md-1">
            <div class="form-group">
                <label for="">Discount</label>
                <input type="number" max="100" name="discount[]" id="discount_`+id+`"
                        class="form-control discount_select"
                        value="0">
            </div>
        </div>
        <!--END DISCOUNT-->
        <!--BEGIN SELLING PRICE-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Final Rate</label>
                <input type="text"  name = "final_rate[]" id="final_rate_`+id+`"
                        class="form-control discount_select" disabled>
            </div>
        </div>
        <!--END SELLING PRICE-->
        
    <!--BEGIN DELETE BUTTON-->
    <div class="col-md-1">
        <button onclick="deleteProduct(`+id+`)"
                type="button"
                class="btn btn-danger"
                style="margin-top: 45%;">
                <i class="far fa-trash-alt"></i>            
        </button>
    </div>
    <!--END DELETE BUTTON-->
</div>
<!--END PRODUCT CUSTOM CONTROL-->`
        );

        
                                
        $.ajax({
            url:baseURL + filePath,
            method: 'POST',
            data:{
                getCategories: true
            },
            dataType: 'json',
            success: function(categories){
                categories.forEach(function (category){
                $("#category_"+id).append(
                    `<option value='${category.id}'>${category.name}</option>`
                );
            });
            }
        });
    }

    // using event delegation
$("#products_container").on('change','.category_select', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var category_id = this.value;
    $.ajax({
        url:baseURL + filePath,
        method: 'POST',
        data:{
            getProductsByCategoryID: true,
            categoryID: category_id
        },
        dataType: 'json',
        success: function(products){
            $("#product_"+element_id).empty();
            var sell_dom =  document.getElementById("selling_price_"+element_id);
            sell_dom.value = 0;
            $("#product_"+element_id).append("<option disabled selected>Select Product</option>");
            products.forEach(function (product){
                $("#product_"+element_id).append(
                    `<option value='${product.id}'>${product.name}</option>`
                );
            });
           
        }
    })
});
$("#products_container").on('change','.product_select', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var selling_id = this.value;
    // console.log(selling_id+"hi");
    $.ajax({
        url:baseURL + filePath,
        method: 'POST',
        data:{
            getSellingRateByID: true,
            sellingID: selling_id
        },
        dataType: 'json',
        success: function(selling_rate){
            console.log("hey");
            var sell_dom =  document.getElementById("selling_price_"+element_id);
            var sell_rate = selling_rate[0]['selling_rate'];
            sell_dom.value = sell_rate;
           
           
        }
    })
});
$("#products_container").on('change','.quantity_select', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var quantity_id =  document.getElementById("product_"+element_id).value;
    console.log(quantity_id);
    $.ajax({
        url:baseURL + filePath,
        method: 'POST',
        data:{
            getQuantityByID: true,
            quantityID: quantity_id
        },
        dataType: 'json',
        success: function(quantity){
            var quantity_dom =  document.getElementById("quantity_"+element_id);
            console.log(quantity[0]['quantity']);
            quantity_dom.max = quantity[0]['quantity'];
            quantity_dom.min = 0;
            if(quantity_dom.value > parseInt(quantity[0]['quantity']))
                quantity_dom.value = quantity[0]['quantity'];

            if(quantity_dom.value < 0)
                quantity_dom.value = 0;
            
            setFinalRate(element_id);
            setFinalTotal();
        }
    })
});
$("#products_container").on('change','.discount_select', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var getDiscount = document.getElementById('discount_'+element_id);
    getDiscount.max = 100;
    getDiscount.min = 0;
    if(getDiscount.value > 100)
        getDiscount.value = 100;

    if(getDiscount.value < 0)
        getDiscount.value = 0;

    setFinalRate(element_id);
    setFinalTotal();
});
function setFinalRate(element_id){
    
    var getFinalRateDom = document.getElementById("final_rate_"+element_id);
    var getQuantityDom = document.getElementById("quantity_"+element_id).value;
    var getSellingPriceDom = document.getElementById("selling_price_"+element_id).value;
    var getDiscountDom = document.getElementById("discount_"+element_id).value;
    getFinalRateDom.value = (getSellingPriceDom * getQuantityDom) - ((getSellingPriceDom * getQuantityDom)/(100/getDiscountDom)).toFixed(2);
}
function setFinalTotal(){
    
    var getFinalTotal = document.getElementById("finalTotal");
    var check = 0;
    var temp=0;
    var i;
  
    for(i = 1; i <= id;i++)
    {
         check = (document.getElementById("final_rate_"+i));
        if(check!=null)
        {
            temp += parseInt((document.getElementById("final_rate_"+i).value));
            // console.log("isha");
           
        }
        
        // console.log(id);
       
    }

    getFinalTotal.value = temp;
    
}

var customer_email = document.getElementById('customer_email');
var check_email = document.getElementById('check_email');

check_email.addEventListener('click',function(e){

    var email_verify_success = document.getElementById('email_verify_success');
    var email_verify_fail = document.getElementById('email_verify_fail');
    var add_customer_btn = document.getElementById('add_customer_btn');
    var customer_id = document.getElementById('customer_id');
    var add_sales = document.getElementById('add_sales');
    var add_product = document.getElementById('add-product');
  
    $.ajax({
        url:baseURL + filePath,
        method: 'POST',
        data:{
            getEmail: true,
            email: customer_email.value
        },
        dataType: 'json',
        success: function(email){ 
            console.log(email);
            if(email.length != 0){
            if(email[0]['email_id'] == customer_email.value){
                email_verify_success.style.display="inline";
                email_verify_fail.style.display ="none";
                add_sales.disabled=false;
                add_product.disabled=false;
                add_customer_btn.style.display ="none";
                customer_id.value = email[0]['id']
            }
        }else{
            email_verify_fail.style.display ="inline";
            add_customer_btn.style.display ="inline";
            add_sales.disabled=true;
            add_product.disabled=true;
           
            email_verify_success.style.display="none";
        }
            
            
        }
    })

});

