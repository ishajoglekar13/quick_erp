<?php
class Sales{
    private $table = "sales";
    private $invoice_table = "invoice";
    protected $di;
    private $database;
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
   
    public function addSales($data,$invoice){
        
    
        try
            {
    
            if($invoice){
                $this->database->beginTransaction();
            //    Util::dd());
                for($i=0;$i<count($data['product_id']);$i++){

                    // Util::Dd($data['discount'][$i]);
                    $data_to_be_inserted['product_id'] = $data['product_id'][$i];
                    $data_to_be_inserted['quantity'] = $data['quantity'][$i];
                    $data_to_be_inserted['discount'] = $data['discount'][$i];
                    $data_to_be_inserted['invoice_id'] = $invoice;
                    // Util::dd($data_to_be_inserted);
                    $sales_id =$this->database->insert($this->table,$data_to_be_inserted);
                }
                $this->database->commit();
                return ADD_SUCCESS;
            }
        }catch(Exception $e){
            $this->database->rollBack();
            return ADD_ERROR;
            }
        

    }

    public function addInvoice($data){

        try
        {
        $this->database->beginTransaction();
        
        $data_to_be_inserted = ['customer_id' => $data['customer_id']];
        $invoice_id =$this->database->insert($this->invoice_table,$data_to_be_inserted);
        
        $this->database->commit();
        return $invoice_id;
    }
    catch(Exception $e){
        $this->database->rollBack();
        return ADD_ERROR;
        }
        
    }

    public function getSales(){
        $query = "SELECT count(*) as count from sales where created_at >= CURDATE() - INTERVAL 6 MONTH";
        $result = $this->database->raw($query,PDO::FETCH_ASSOC);
        // Util::dd($result[0]['count']);
        $this->di->salesCount = $result[0]['count'];

        $query = "SELECT sales.product_id,selling_rate,quantity,discount,MONTH(sales.created_at)as month,products_selling_rate.with_effect_from from products_selling_rate INNER JOIN sales on products_selling_rate.product_id = sales.product_id  and with_effect_from <= sales.created_at and sales.created_at >= CURDATE() - INTERVAL 5 MONTH group by (sales.created_at)";
        $result1 = $this->database->raw($query,PDO::FETCH_ASSOC);

        // Util::Dd($result1);

        $arrayPurchases = array();
 
        

        $finalRate = 0;$temp=0;
        for($i=0;$i<count($result1);$i++){
            
            
                $finalRate = ((int)$result1[$i]['selling_rate']*(int)$result1[$i]['quantity']) - (((int)$result1[$i]['selling_rate']*(int)$result1[$i]['quantity'])/(100/(int)$result1[$i]['discount']));
            while($result1[$i]['month'] == $result1[$i+1]['month'])
                {

                    $finalRate += ((int)$result1[$i+1]['selling_rate']*(int)$result1[$i+1]['quantity']) - (((int)$result1[$i+1]['selling_rate']*(int)$result1[$i+1]['quantity'])/(100/(int)$result1[$i+1]['discount']));

                   
                    $i++;
                }
                
            array_push($arrayPurchases,$finalRate);    
            // Util::dd($finalRate);
        }
        $this->di->salesArray = $arrayPurchases;

        // Util::Dd($this->di->salesArray);
        // return $result[0]['count'];
        
    }



  
}
?>