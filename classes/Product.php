<?php
class Product
{
    private $table = "products";
    protected $di;
    private $database;
    private $validator;
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function getValidator(){
        return $this->validator;
    }
    public function validateData($data)
    {
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data,[
            'name'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>20,

            ],'specification'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>50

            ],'hsn_no'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>0,

            ],'eoq_level'=>[
                'required'=>true,
                'maxlength'=>20,
                

            ],'quantity'=>[
                'required'=>true,
                'minlength'=>1,
                'maxlength'=>20,
            ],
            
        ]);
        
    }

    public function addProduct($data)
    {
        // Util::dd($data);
        //VALIDATE DATA
        $this->validateData($data);

        //INSERT DATA IN DATABASE
        if(!$this->validator->fails())
        {
            try{

                $table_attr = ['name'=>0,'specification'=>0,'hsn_code'=>0,'category_id'=>0,'eoq_level'=>0,'danger_level'=>0];
                $data_to_be_inserted = array_intersect_key($data,$table_attr);
                $data_to_be_inserted['quantity']=0;
                //Util::dd($data_to_be_inserted);
                $this->database->beginTransaction();
               
                $product_id = $this->database->insert($this->table,$data_to_be_inserted);
                //$address_id = $this->database->insert($this->address_table,$address_to_be_inserted);

                // Util::dd($product_id);
                $data_for_product_supplier = [];
                $data_for_product_supplier['product_id']= $product_id;
                foreach($data['supplier_id'] as $supplier_id){
                    $data_for_product_supplier['supplier_id'] = $supplier_id;
                    //Util::dd($data_for_product_supplier);
                    $this->database->insert('product_supplier',$data_for_product_supplier);
                }
                //Util::dd("$data_for_product_supplier");
                $data_for_selling_table = [];
                $data_for_selling_table['product_id']= $product_id;
                $data_for_selling_table['selling_rate']= $data['selling_rate'];
                $this->database->insert('products_selling_rate',$data_for_selling_table);
                 //Util::dd("hmm");
                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                Util::dd($e);
                $this->database->rollBack();
                return ADD_ERROR;
            }


        }
        else
        {
            return VALIDATION_ERROR;
        }
    }

    // public function getProductByProductId($prod_id,$fetchMode = PDO::FETCH_ASSOC){
    //     $query = "Select products.name, products.specification, products.hsn_code, products.eoq_level, products.danger_level, products_selling_rate.selling_rate,products_selling_rate.with_effect_from as wef, category.name as category_name, concat(suppliers.first_name,\" \",suppliers.last_name) as supplier_name from  products inner JOIN product_supplier ON products.id = product_supplier.product_id inner join products_selling_rate on products.id  = products_selling_rate.product_id INNER join category on products.category_id = category.id INNER JOIN suppliers on product_supplier.supplier_id = suppliers.id where products.id ={$prod_id} and products.deleted = 0";
    //     $result = $this->database->raw($query,$fetchMode);
    //     return $result;
    // }

    public function getProductByProductId($prod_id,$fetchMode = PDO::FETCH_ASSOC){
        $query = "Select products.name, products.specification, products.hsn_code, products.eoq_level, products.danger_level, products_selling_rate.selling_rate, category.name as category_name, concat(suppliers.first_name,\" \",suppliers.last_name) as supplier_name, products_selling_rate.with_effect_from as wef FROM products_selling_rate INNER JOIN (SELECT product_id, MAX(with_effect_from) as wef from (SELECT product_id, with_effect_from FROM products_selling_rate WHERE with_effect_from<=CURRENT_TIMESTAMP) as temp GROUP BY temp.product_id) as max_date_table ON max_date_table.product_id = products_selling_rate.product_id AND products_selling_rate.with_effect_from = max_date_table.wef INNER JOIN products ON products.id = products_selling_rate.product_id inner JOIN product_supplier ON products.id = product_supplier.product_id INNER join category on products.category_id = category.id INNER JOIN suppliers on product_supplier.supplier_id = suppliers.id where products.id = {$prod_id} and products.deleted = 0";
        $result = $this->database->raw($query,$fetchMode);
        return $result;
    }

    public function getPuchaseRateByID($prod_id,$fetchMode = PDO::FETCH_ASSOC){
        $query = "SELECT selling_rate, MAX(with_effect_from) as wef from products_selling_rate where product_id = {$prod_id} and with_effect_from<=CURRENT_TIMESTAMP";
        $result = $this->database->raw($query,$fetchMode)[0]['selling_rate'];
        
        return $result;
    }
    

 

    public function getJSONDataForDataTable($draw, $search_parameter,$order_by,$start,$length)
    {
        $columns = ['products.id','products.name','products.specification','products.quantity','products_selling_rate.selling_rate',
                'products_selling_rate.with_effect_from','category.name','products.eoq_level','products.danger_level'];

        $query = "SELECT products.id, products.name as product_name, products.specification, products.quantity, products.eoq_level, products.danger_level, category.name as category_name, GROUP_CONCAT(CONCAT(suppliers.first_name,\" \",suppliers.last_name) SEPARATOR ' | ') as supplier_name, products_selling_rate.selling_rate, products_selling_rate.with_effect_from FROM products_selling_rate INNER JOIN (SELECT product_id, MAX(with_effect_from) as wef FROM ( SELECT product_id, with_effect_from FROM products_selling_rate WHERE with_effect_from<=CURRENT_TIMESTAMP) as temp GROUP BY temp.product_id) as max_date_table ON max_date_table.product_id = products_selling_rate.product_id AND products_selling_rate.with_effect_from = max_date_table.wef INNER JOIN products ON products.id = products_selling_rate.product_id INNER JOIN category ON category.id = products.category_id INNER JOIN product_supplier ON product_supplier.product_id = products.id INNER JOIN suppliers ON suppliers.id = product_supplier.supplier_id WHERE products.deleted = 0 ";

        $groupBy = " GROUP BY products.id";

        $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM ((SELECT products.id  FROM products_selling_rate INNER JOIN (SELECT product_id, MAX(with_effect_from) as wef FROM (SELECT product_id, with_effect_from FROM products_selling_rate WHERE with_effect_from<=CURRENT_TIMESTAMP) as temp GROUP BY temp.product_id) as max_date_table ON max_date_table.product_id = products_selling_rate.product_id AND products_selling_rate.with_effect_from = max_date_table.wef INNER JOIN products ON products.id = products_selling_rate.product_id INNER JOIN category ON category.id = products.category_id INNER JOIN product_supplier ON product_supplier.product_id = products.id INNER JOIN suppliers ON suppliers.id = product_supplier.supplier_id WHERE products.deleted = 0 GROUP BY products.id) as final_table)";

        $filteredRowCountQuery = "SELECT COUNT(*) as total_count FROM ((SELECT products.id  FROM products_selling_rate INNER JOIN (SELECT product_id, MAX(with_effect_from) as wef FROM (SELECT product_id, with_effect_from FROM products_selling_rate WHERE with_effect_from<=CURRENT_TIMESTAMP) as temp GROUP BY temp.product_id) as max_date_table ON max_date_table.product_id = products_selling_rate.product_id AND products_selling_rate.with_effect_from = max_date_table.wef INNER JOIN products ON products.id = products_selling_rate.product_id INNER JOIN category ON category.id = products.category_id INNER JOIN product_supplier ON product_supplier.product_id = products.id INNER JOIN suppliers ON suppliers.id = product_supplier.supplier_id WHERE products.deleted = 0";
        
        $endPart = " GROUP BY products.id) as final_table)";

        if($search_parameter != null)
        {   
            $condition = " AND products.name LIKE '%{$search_parameter}%' OR products.specification LIKE '%{$search_parameter}%' OR category.name LIKE '%{$search_parameter}%' OR suppliers.first_name LIKE '%{$search_parameter}%' OR suppliers.last_name LIKE '%{$search_parameter}%'";
            $query .= " $condition";
            $filteredRowCountQuery .= " $condition";
        }
 
        $query .= $groupBy;
        $filteredRowCountQuery .= $endPart;
        if($order_by != null)
        {
            $query .= " ORDER BY {$columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
            //$filteredRowCountQuery .= " ORDER BY {$columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
        }
        else{
            $query .= " ORDER BY {$columns[0]} ASC";
            //$filteredRowCountQuery .= " ORDER BY {$columns[0]} ASC";
        }
        if($length != -1)
        {
            $query .= " LIMIT {$start}, {$length}";
        }
        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;
        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count : 0;
        $fetchedData = $this->database->raw($query);
        $data = [];
        $numRows = is_array($fetchedData) ? count($fetchedData) : 0;
        $basePages = BASEPAGES;
        for($i=0; $i<$numRows; $i++)
        {
            $subArray = [];
            $subArray[] = $start+$i+1;
            $subArray[] = $fetchedData[$i]->product_name;
            $subArray[] = $fetchedData[$i]->specification;
            $subArray[] = $fetchedData[$i]->quantity;
            $subArray[] = $fetchedData[$i]->selling_rate;
            $subArray[] = $fetchedData[$i]->with_effect_from;
            $subArray[] = $fetchedData[$i]->category_name;
            $subArray[] = $fetchedData[$i]->eoq_level;
            $subArray[] = $fetchedData[$i]->danger_level;
            $subArray[] = $fetchedData[$i]->supplier_name;
            $subArray[] = <<<BUTTONS
<a href="{$basePages}edit-product.php?id={$fetchedData[$i]->id}" class='btn btn-outline-primary btn-sm'>
<i class="fas fa-pencil-alt"></i>
</a>
<button class='btn btn-outline-danger btn-sm delete' data-id='{$fetchedData[$i]->id}' data-toggle='modal' data-target='#deleteModal'><i class="fas fa-trash-alt"></i></button>       
BUTTONS;
            $data[] = $subArray;
        }
        $output = array(
            'draw'=>$draw,   //gives page no.
            'recordsTotal'=>$numberOfTotalRows,
            'recordsFiltered'=>$numberOfFilteredRows,
            'data'=>$data
        );
        echo json_encode($output);
    } 


    public function getProductByID($id){
        return $this->database->readData('products',[],"id = {$id} and deleted = 0");
    }



    public function getProductsByCategoryID($category_id){
        return $this->database->readData('products',['id','name'],"category_id = {$category_id} and deleted = 0");
    }
    public function getSellingRateByID($product_id){
       
        $query = "select t1.product_id,t1.selling_rate,t1.with_effect_from FROM products_selling_rate t1 INNER JOIN(SELECT product_id,selling_rate,MAX(with_effect_from) as wef from products_selling_rate where with_effect_from <= CURRENT_TIMESTAMP GROUP BY product_id HAVING product_id = {$product_id}) t2 on t2.wef = t1.with_effect_from and t1.product_id = {$product_id}";
         return $this->database->raw($query,PDO::FETCH_ASSOC);
    }
    public function getQuantityByID($product_id){
        //Util::dd("hi");
        return $this->database->readData('products',['quantity'],"id = {$product_id}");
    }

    public function update($data,$id)
    {
    
        // Util::dd($data);
        $data_to_be_updated_products['name'] = $data['name'];
        $data_to_be_updated_products['specification'] = $data['specification'];
        $data_to_be_updated_products['eoq_level'] = $data['eoq_level'];
        $data_to_be_updated_products['danger_level'] = $data['danger_level'];
        
        $data_to_be_updated_products_selling['selling_rate'] = $data['selling_rate'];
        $data_to_be_updated_products_selling['product_id'] = $id;
        $data_to_be_updated_products_selling['with_effect_from'] = $data['wef'];

        $data_to_be_updated_products_supplier['supplier_id'] = $data['supplier_id'];


        $this->ValidateEditData($data_to_be_updated_products);
        $this->ValidateSellingPriceData($data_to_be_updated_products_selling);
        
        if(!$this->validator->fails())
        {
            
            try{
                $this->database->beginTransaction();
               
                $this->database->update($this->table,$data_to_be_updated_products,"id = {$id}");
                
                $this->database->insert('products_selling_rate',$data_to_be_updated_products_selling);
                
                $this->database->deletePermanently('product_supplier',"product_id = {$id}");
                // Util::dd("hey"); 
                
                foreach($data['supplier_id'] as $supplier){
                    // Util::Dd($supplier);
                    $data_for_product_supplier['supplier_id'] = $supplier;
                    $data_for_product_supplier['product_id'] = $id;
                    $this->database->insert('product_supplier',$data_for_product_supplier);
                }
                $this->database->commit();
                
                return UPDATE_SUCCESS;
            }catch(Exception $e){
                $this->database->rollBack();
                return UPDATE_ERROR;
            }
        }
        else{
            return VALIDATION_ERROR;
        }
        
    }

    public function ValidateEditData($data)
    {
       
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data,[
            'name'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>20
                

            ],'specification'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>50

            ],'eoq_level'=>[
                'required'=>true,
               
                'maxlength'=>3,
                

            ],'danger_level'=>[
                'required'=>true,
                
                'maxlength'=>3,             

            ]
            
        ]);
        
        
    }


    public function ValidateSellingPriceData($data)
    {
        // Util::dd(s$data);
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data,[
            'selling_price'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>20,             
            
            ],
            
        ]);
        
    }

}
