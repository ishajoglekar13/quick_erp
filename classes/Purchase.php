<?php
class Purchase
{
    private $table = "purchases";
    protected $di;
    private $database;
    public static $db;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
        $db = $this->di->get('database');
    }
   

    public function addPurchases($data)
    {
        try{

            $this->database->beginTransaction();
            //    Util::dd());
                for($i=0;$i<count($data['product_id']);$i++){

                    // Util::Dd($data['discount'][$i]);
                    $data_to_be_inserted['product_id'] = $data['product_id'][$i];
                    $data_to_be_inserted['supplier_id'] = $data['supplier_id'][$i];
                    $data_to_be_inserted['purchase_rate'] = $data['purchase_rate'][$i];
                    $data_to_be_inserted['quantity'] = $data['quantity'][$i];
                    
                    // Util::dd($data_to_be_inserted);
                    $purchase_id =$this->database->insert($this->table,$data_to_be_inserted);
                    
                }
                $this->database->commit();
                return ADD_SUCCESS;
        }
        catch(Exception $e)
        {
            Util::dd($e);
            $this->database->rollBack();
            return ADD_ERROR;
        }


    }

    public function getPurchases(){
        $query = "SELECT count(*) as count from purchases where created_at >= CURDATE() - INTERVAL 6 MONTH";
        $result = $this->database->raw($query,PDO::FETCH_ASSOC);
        
        $this->di->purchasesCount = (int)$result[0]['count'];

        $query1 ="SELECT SUM(purchase_rate*quantity) as sum,MONTH(created_at) as month from purchases where created_at >= CURDATE() - INTERVAL 5 MONTH Group By MONTH(created_at)";
        $result1 = $this->database->raw($query1,PDO::FETCH_ASSOC);
        $arrayPurchases = array();


        for($i=0;$i<count($result1);$i++){
            array_push($arrayPurchases,(int)$result1[$i]['sum']);    
        }
        $this->di->purchasesArray = $arrayPurchases;

        // Util::Dd($this->di->purchasesArray);
        
        
    }



}
